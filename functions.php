<?php 
function htmlEncode($s){
    return isset($s)? htmlentities(htmlspecialchars($s)):'';
}
function htmlDecode($s){
    return isset($s)?  str_replace('\\', '&bsol;',str_replace("'", '&apos;',str_replace('"', '&quot;',html_entity_decode(htmlspecialchars_decode($s))))):'';
}

function isValid($v){
    if(isset($v) && $v != null){
        return true;
    }else{
        return false;
    }
}
function isUserConnected(){
    if(isset($_SESSION["user"])){
        return true;
    }
    return false;
}
function clean($str, $charset='utf-8') {
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    $str = str_replace(' ', '-', $str);
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    return preg_replace('#[^A-Za-z0-9\-]#', '', $str);   // or add this : mb_strtoupper($str); for uppercase :)
 }
 /**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mp | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
function get_gravatar( $email, $s = 80, $d = 'mp', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

function imageResize($imageResourceId,$width,$height) {


    $targetWidth = $GLOBALS["config"]["Avatars"]["width"];
    $targetHeight = $GLOBALS["config"]["Avatars"]["height"];


    $targetLayer=imagecreatetruecolor($targetWidth,$targetHeight);
    imagecopyresampled($targetLayer,$imageResourceId,0,0,0,0,$targetWidth,$targetHeight, $width,$height);


    return $targetLayer;
}

function imagecreatefromfile( $filename, $type ) {
    if (!file_exists($filename)) {
        throw new InvalidArgumentException('File "'.$filename.'" not found.');
    }
    switch ( $type ) {
        case 'jpeg':
        case 'jpg':
            return imagecreatefromjpeg($filename);
        break;

        case 'png':
            return imagecreatefrompng($filename);
        break;

        case 'gif':
            return imagecreatefromgif($filename);
        break;

        default:
            throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');
        break;
    }
}