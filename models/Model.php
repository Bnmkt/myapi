<?php

class Model{

    function __construct(){

    }
    function getDbConnection(){
        $db = parse_ini_file("db.ini");
        $db_host = $db["HOST"];
        $db_name = $db["NAME"];
        $db_user = $db["USER"];
        $db_pass = $db["PASS"];
        try {
            $conn = new PDO("mysql:host=localhost;dbname=my_api", "bnmkt", "123456");
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ); 
            $conn->exec("set names utf8");
          } catch(PDOException $e) {
            print_r($e->getMessage());
          }
          return $conn;
    }
    function getLastId(){
      $db = $this->getDbConnection();
      return $db->lastInsertId();
    }
}