<?php

require_once("Model.php");

class Auth extends Model{
    function __construct(){
        parent::__construct();
    }
    function register($email, $password){
        $db = $this->getDbConnection();
        $username = "User_".strtoupper(base_convert(hexdec(uniqid()), 10, 35));
        try {
            $query = $db->prepare("INSERT INTO users (email, password, username) VALUES (:email, :password, :username)");
            return $query->execute([
                ":email"=>$email,
                ":password"=>$password,
                ":username"=>$username
            ]);
        } Catch(PDOException $e){
            return $e;
        }
    }
    function connect($email){
        $db = $this->getDbConnection();
        try {
            $query = $db->prepare("SELECT * FROM users WHERE email=:email");
            $query->execute([
                ":email"=>$email
            ]);
            return $query->fetch();
        } catch (PDOException $e) {
            return $e;
        }
    }
    function getUserData($id){
        $db = $this->getDbConnection();
        try {
            $query = $db->prepare("SELECT * FROM users WHERE id=:id");
            $query->execute([
                ":id"=>$id
            ]);
            return $query->fetch();
        } catch (PDOException $e) {
            return $e;
        }
    }
    function update($username, $password, $options, $userid){
        $db = $this->getDbConnection();
        $reqPlus = "";
        $vars = [
            ":username"=>$username,
            ":options"=>$options,
            ":userid"=>$userid
        ];
        if($password != null){
            $reqPlus = ", password=:password";
            $vars[":password"] = $password;
        }
        try {
            $query = $db->prepare("UPDATE users SET username=:username $reqPlus, options=:options WHERE id=:userid");
            return $query->execute($vars);
        } catch (PDOException $e) {
            return $e;
        }
    }
}

/*


*/