<?php

require_once("Model.php");

class Api extends Model{
    function __construct(){
        parent::__construct();
    }
    function getApiByIdSlug($id, $slug){
        if(isset($id) && isset($slug)){
            $db = $this->getDbConnection();
            $query = $db->prepare("SELECT * FROM apis WHERE id = :id AND slug = :slug  ORDER BY ID DESC");
            $query->execute([
                ":id"=>$id,
                ":slug"=>$slug
            ]);
            return $query->fetch();
        }
        return 0;
    }
    function getApi($id){
        if(isset($id)){
            $db = $this->getDbConnection();
            $query = $db->prepare("SELECT * FROM apis WHERE id = :id");
            $query->execute([
                ":id"=>$id
            ]);
            return $query->fetch();
        }
        return 0;
    }
    function getTable($id){
        if(isset($id)){
            $db = $this->getDbConnection();
            $query = $db->prepare("SELECT * FROM api_tables WHERE id = :id");
            $query->execute([
                ":id"=>$id
            ]);
            return $query->fetch();
        }
        return 0;
    }
    function getRow($id){
        if(isset($id)){
            $db = $this->getDbConnection();
            $query = $db->prepare("SELECT * FROM api_rows WHERE id = :id");
            $query->execute([
                ":id"=>$id
            ]);
            return $query->fetch();
        }
        return 0;
    }
    function getUserApi($userid, $id = null){
        $db = $this->getDbConnection();
        $var = [":uid"=>$userid];
        $queryPlus = '';
        if(isset($id)){
            $queryPlus = " AND id = :id";
            array_push($var, $id);
        }
        $query = $db->prepare("SELECT * FROM apis WHERE user_id = :uid ORDER BY update_date DESC");
        $query->execute($var);
        return $query->fetchAll();
    }
    function getTables($api_id, $field = null){
        $db = $this->getDbConnection();
        $var = [":apid"=>$api_id];
        $queryPlus = "";
        if(isset($field)){
            $var = array_merge($var, [":value"=>$field]);
            $queryPlus = "AND (slug = :value OR id = :value)";
        }
        $query = $db->prepare("SELECT * FROM api_tables WHERE api_id = :apid $queryPlus ORDER BY ID DESC");
        $query->execute($var);
        return $query->fetchAll();
    }
    function getRows($table_id, $value = null){
        $db = $this->getDbConnection();
        $var = [":tableid"=>$table_id];
        $queryPlus = "";
        if(isset($value)){
            $var = array_merge($var, [":value"=>$value]);
            $queryPlus = "AND (slug = :value OR id = :value)";
        }
        $query = $db->prepare("SELECT * FROM api_rows WHERE table_id = :tableid $queryPlus ORDER BY ID DESC");
        $query->execute($var);
        return $query->fetchAll();
    }
    function getValues($row_id, $id = null){
        $db = $this->getDbConnection();
        $var = [":rowid"=>$row_id];
        $queryPlus = "";
        if(isset($id)){
            $var = array_merge($var, [":value"=>$id]);
            $queryPlus = "AND (id = :value)";
        }
        $query = $db->prepare("SELECT * FROM api_values WHERE row_id = :rowid $queryPlus ORDER BY ID DESC");
        $query->execute($var);
        return $query->fetchAll();
    }
    function createApi($name, $description, $keywords, $userid, $cluster){
        $host = $cluster->host;
        $database = $cluster->db;
        $db = $this->getDbConnection();
        $slug = implode("_", explode(" ", strtolower(clean($name))));
        $query = $db->prepare("INSERT INTO apis (name, description, keywords, user_id, slug) VALUES(:name, :description, :keywords, :userid, :slug)");
        $query->execute([
            ":name"=>$name,
            ":description"=>$description,
            ":keywords"=>$keywords,
            ":userid"=>$userid,
            ":slug"=>$slug
        ]);
        return $db->lastInsertId();
    }
    function createTable($name, $description, $apid){
        $db = $this->getDbConnection();
        $slug = implode("_", explode(" ", strtolower(clean($name))));
        $query = $db->prepare("INSERT INTO api_tables (api_id,name, description, slug) VALUES(:apid, :name, :description, :slug);
        Update apis set update_date = CURRENT_TIMESTAMP where id = :apid;");
        $query->execute([
            ":name"=>$name,
            ":description"=>$description,
            ":apid"=>$apid,
            ":slug"=>$slug
        ]);
        return $db->lastInsertId();
    }    
    function createRow($name, $description, $tableid){
        $db = $this->getDbConnection();
        $slug = implode("_", explode(" ", strtolower(clean($name))));
        $query = $db->prepare("INSERT INTO api_rows (table_id,name, description, slug) VALUES(:tableid, :name, :description, :slug);
        Update apis set update_date = CURRENT_TIMESTAMP where id in (SELECT api_id FROM api_tables WHERE id=:tableid);");
        $query->execute([
            ":name"=>$name,
            ":description"=>$description,
            ":tableid"=>$tableid,
            ":slug"=>$slug
        ]);
        return $db->lastInsertId();
    }
    function createValue($value, $rowid){
        $db = $this->getDbConnection();
        $query = $db->prepare("INSERT INTO api_values (row_id, value) VALUES(:rowid, :value);
        Update apis set update_date = CURRENT_TIMESTAMP where id in (SELECT api_id FROM api_tables WHERE id IN 
        (SELECT table_id FROM api_rows WHERE id = :rowid));");
        $query->execute([
            ":value"=>$value,
            ":rowid"=>$rowid,
        ]);
        return $db->lastInsertId();
    }
    function updateApi($name, $description, $keywords, $slug, $userId, $id){
        $db = $this->getDbConnection();
        $query = $db->prepare("UPDATE apis SET name=:name, description=:description, keywords=:keywords, slug=:slug, update_date=CURRENT_TIMESTAMP WHERE id=:id AND user_id=:userid");
        $query->execute([
            ":name"=>$name,
            ":description"=>$description,
            ":keywords"=>$keywords,
            ":slug"=>$slug,
            ":id"=>$id,
            ":userid"=>$userId
        ]);
    }
    function updateTable($name, $description, $slug, $userId, $id){
        $db = $this->getDbConnection();
        $query = $db->prepare("UPDATE api_tables AS tables INNER JOIN apis ON apis.id = tables.api_id AND apis.user_id=:userid SET tables.name=:name, tables.description=:description, tables.slug=:slug, apis.update_date=CURRENT_TIMESTAMP WHERE tables.id=:id");
        $query->execute([
            ":name"=>$name,
            ":description"=>$description,
            ":slug"=>$slug,
            ":id"=>$id,
            ":userid"=>$userId
        ]);
    }
    function updateRow($name, $description, $slug, $userId, $id){
        $db = $this->getDbConnection();
        $query = $db->prepare(" UPDATE api_rows AS row INNER JOIN api_tables as tab ON tab.id = row.table_id INNER JOIN apis as api ON api.id = tab.api_id AND api.user_id = :userid
                                SET row.name=:name, row.description=:description, row.slug=:slug, api.update_date = CURRENT_TIMESTAMP WHERE row.id=:id");
        $query->execute([
            ":name"=>$name,
            ":description"=>$description,
            ":slug"=>$slug,
            ":id"=>$id,
            ":userid"=>$userId
        ]);
    }
    function updateValue($value, $id, $userId){
        $db = $this->getDbConnection();
        $query = $db->prepare(" UPDATE api_values as val 
                                    INNER JOIN api_rows AS row ON row.id = val.row_id
                                        INNER JOIN api_tables as tab ON tab.id = row.table_id
                                                INNER JOIN apis as api ON api.id = tab.api_id AND api.user_id = :userid
                                SET val.value=:value, api.update_date = CURRENT_TIMESTAMP
                                WHERE val.id=:id");
        $query->execute([
            ":value"=>$value,
            ":id"=>$id,
            ":userid"=>$userId
        ]);
    }
    function deleteValue($id, $user_id){
        echo "MODEL > supression de la value $id\n";
        $db = $this->getDbConnection();
        $query = $db->prepare("DELETE FROM api_values WHERE
                                row_id IN ( SELECT id FROM api_rows WHERE
                                    table_id IN ( SELECT id FROM api_tables WHERE
                                        api_id IN ( SELECT id FROM apis WHERE user_id = :userid ))) 
                                AND (api_values.id = :id)");
        $query->execute([
            ":id"=>$id,
            ":userid"=>$user_id
        ]);
    }
    function deleteRow($id, $user_id){
        echo "MODEL > supression de la row $id\n";
        $db = $this->getDbConnection();
        $query = $db->prepare("DELETE FROM api_rows WHERE
                                table_id IN ( SELECT id FROM api_tables WHERE
                                    api_id IN ( SELECT id FROM apis WHERE user_id = :userid )) 
                                AND (api_rows.id = :id)");
        $query->execute([
            ":id"=>$id,
            ":userid"=>$user_id
        ]);
    }
    function deleteTable($id, $user_id){
        echo "MODEL > supression de la table $id\n";
        $db = $this->getDbConnection();
        $query = $db->prepare("DELETE FROM api_tables WHERE
                                api_id IN ( SELECT id FROM apis WHERE user_id = :userid ) 
                               AND (api_tables.id = :id)");
        $query->execute([
            ":id"=>$id,
            ":userid"=>$user_id
        ]);
        return "DELETE FROM api_tables WHERE
        api_id IN ( SELECT id FROM apis WHERE user_id = $user_id ) 
        AND (api_tables.id = $id)";
    }
    function deleteApi($id, $user_id){
        echo "MODEL > supression de la api $id\n";
        $db = $this->getDbConnection();
        $query = $db->prepare("DELETE FROM apis WHERE user_id = :userid AND id = :id");
        $query->execute([
            ":id"=>$id,
            ":userid"=>$user_id
        ]);
    }
    function countByUserId($user_id){
        $db = $this->getDbConnection();
        $query = $db->prepare("SELECT COUNT(id) as total FROM apis WHERE user_id = :userid");
        $query->execute([
            "userid"=>$user_id
        ]);
        return $query->fetch();
    }
    private function connectToHost(){
        
    }
}