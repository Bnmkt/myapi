<?php

require("controllers/Controller.php");
require("models/Auth.php");
require("models/Api.php");


class AuthController extends Controller
{
    function __construct($params){
        parent::__construct($params);
    }
    public function login(){
        if(isUserConnected()){
            header("location:".$GLOBALS["path"]."");
        }
        return [
            "view"=>"auth/login.php"
        ];
    }
    public function logout(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        session_destroy();
        session_unset(); 
        return [
            "view"=>"auth/logout.php"
        ];
    }
    public function signin(){
        if(isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        return [
            "view"=>"auth/signin.php"
        ];
    }
    public function connect($redirect=true){
        if(isUserConnected() && $redirect == true){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $email = $_REQUEST["email"];
        $pass1 = $_REQUEST["password"];
        $error = [];
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) array_push($error, "Bad mail format");
        if($email != "" && $pass1 != "" && sizeof($error) == 0){
            $auth = new Auth();
            $connection = $auth->connect($email);            
            if($connection != false){
                if(password_verify($pass1, $connection->password)){
                    $_SESSION["user"] = [];
                    $_SESSION["user"]["id"] = $connection->id;
                    $_SESSION["user"]["username"] = $connection->username;
                    $_SESSION["user"]["email"] = $connection->email;
                    if($redirect == true){
                        header("location:".$GLOBALS["path"]."home/view");
                        die();
                    }
                }else{
                    array_push($error, "Bad password");
                }
            }else{
                array_push($error, "Email $email is not registered");
                return [
                    "view"=>"auth/signin.php",
                    "error"=>$error,
                    "email"=>$email,
                    "pass"=>$pass1
                ];
            }
        }
        return [
            "view"=>"auth/login.php",
            "error"=> $error
        ];
    }
    public function updateData(){

    }
    public function register(){
        if(isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $email = $_REQUEST["email"];
        $pass1 = $_REQUEST["password"];
        $pass2 = $_REQUEST["password2"];
        $error = [];
        if($email != "" && $pass1 != ""){
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) array_push($error, "Bad mail format");
            if(strlen($pass1)<=3) array_push($error, "Passwords should at least have 3 characters");
            if($pass1!=$pass2) array_push($error, "Passwords does not match");
            if(sizeof($error) === 0){
                $auth = new Auth();
                $pass = password_hash($pass1, PASSWORD_BCRYPT);
                if($auth->register($email, $pass)){
                    $this->connect();
                    print_r($auth->register($email, $pass));
                    //header("location:".$GLOBALS["path"]."home/view");
                    die();
                }else{
                    array_push($error, "Email $email is already registered");
                }
            }
        }
        return [
            "view"=>"auth/signin.php",
            "error"=> $error
        ];
    }
    public function edit(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $userid = $_SESSION["user"]["id"];
        $auth = new Auth();
        $api = new Api();
        $userData = $auth->getUserData($userid);
        $apiCount = $api->countByUserId($userid);
        $avatarFolder = $GLOBALS["config"]["Avatars"]["folder"];
        $folderName = base_convert($userid % (36*36),10, 36);
        $path = "$avatarFolder/$folderName";
        $userData->avatar = $GLOBALS["path"].$path."/".$userid.".WEBP";
        return [
            "view"=>"auth/edit.php",
            "breadcumb"=>[
                "Edit profile"=>"auth/edit"
            ],
            "user"=>$userData,
            "api"=>$apiCount
        ];
    }
    public function editProfile(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $error = [];
        $auth = new Auth();
        $username = $_REQUEST["username"];
        $avatar = isset($_FILES["avatar"])&&$_FILES["avatar"]["size"]>0?$_FILES["avatar"]:null;
        $visibility = $_REQUEST["visibility"] || false;
        $pass = null;
        $options = "visibility=$visibility&";
        if(isset($_REQUEST["password"]) && strlen($_REQUEST["password"]) > 1 && isset($_REQUEST["password2"]) && strlen($_REQUEST["password2"]) > 1){
            $pass = $_REQUEST["password"];
            $pass2 = $_REQUEST["password2"];
            if($pass1 == $pass2){
                $pass = password_hash($pass1, PASSWORD_BCRYPT);
            }else{
                array_push($error, "Les mots de passe ne sont pas les mêmes");
            }
        }
        if($avatar!=null){
            $userid = $_SESSION["user"]["id"];
            $avatarFolder = $GLOBALS["config"]["Avatars"]["folder"];
            $folderName = base_convert($userid % (36*36),10, 36);
            $path = "$avatarFolder/$folderName";
            if(!is_dir($path)){
                if(!mkdir($path, 0777, true)){
                    die("failed to create folder");
                }
            }
            $tempName = $avatar["tmp_name"];
            $name = $avatar["name"];
            $type = strtolower(pathinfo($name,PATHINFO_EXTENSION));
            $size = $avatar["size"];
            $sourceProperties = getimagesize($tempName);
            $max_size = $GLOBALS["config"]["Files"]["max_size"]*1024;
            $targetWidth = $GLOBALS["config"]["Avatars"]["width"];
            $targetHeight = $GLOBALS["config"]["Avatars"]["height"];
            if(array_search($type, $GLOBALS["config"]["Files"]["types"])===false){
                array_push($error, "$type file type is not an image type");
            }
            if($size>$max_size){
                array_push($error, "File is too large ($size / $max_size kb)");
            }
            if(sizeof($error)==0){
                $imageResourceId = imagecreatefromfile($tempName, $type); 
                $image_p = imagecreatetruecolor($targetWidth, $targetHeight);
                $iw = $sourceProperties[0];
                $ih = $sourceProperties[1];
                $s = $iw < $ih ? $iw : $ih;
                $crop = imagecrop($imageResourceId, ["x"=>0, "y"=>0, "width"=>$s, "height"=>$s]);
                imagecopyresampled($image_p, $crop, 0, 0, 0, 0, $targetWidth, $targetHeight, $s, $s);
                if (imagewebp($image_p, $path."/$userid.webp")) {
                    array_push($error, "Avatar uploaded");
                  } else {
                    array_push($error, "error...");
                  }
            }
        }
        if(sizeof($error) == 0){
            $auth->update($username, $pass, $options, $_SESSION["user"]["id"]);
            $data = $auth->getUserData($_SESSION["user"]["id"]);
            $_SESSION["user"]["username"] = $data->username;
        }
        $edit = $this->edit();
        array_push($error, "profil modifié fdp");
        $edit["error"] = $error;
        return $edit;
    }
}
