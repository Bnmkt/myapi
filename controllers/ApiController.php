<?php

require("controllers/Controller.php");
require("models/Api.php");


class ApiController extends Controller
{
    function __construct($params){
        parent::__construct($params);
    }
    function get(){
        $api = new Api();
        $idslug = explode("-",$this->params[1]);
        $id = $idslug[0];
        $field = strlen($this->params[2])>0?htmlEncode($this->params[2]):null;
        $field_value = strlen($this->params[3])>0?htmlEncode($this->params[3]):null;
        $field_sub_value = strlen($this->params[4])>0?htmlEncode($this->params[4]):null;
        $fieldSlug = null;
        $rowSlug = null;
        array_shift($idslug);
        $slug = implode("-", $idslug);
        $userApi = $api->getApiByIdSlug($id, $slug);
        $tables = $api->getTables($id, $field);
        $renderTables = [];
        $returnedJson = [];
        foreach ($tables as $table){
            //array_concat($renderTables, [$table->slug=>$table]);
            $rows = $api->getRows($table->id, $field_value);
            $renderRows = [];
            unset($table->options);
            if(isset($field)){
                $fieldSlug = $table->slug;
                foreach($rows as $row){
                    $values = $api->getvalues($row->id, $field_sub_value);
                    unset($row->options);
                    $renderValues = [];
                    if(isset($field_value)){
                        $rowSlug = $row->slug;
                        foreach($values as $value){
                            if(isset($field_sub_value)){
                                $value->{$rowSlug} = $value->value;
                                $renderValues = [$value->id=>$value];
                            }else{
                                $renderValues = array_merge($renderValues, [$value->id]);
                            }
                        }
                        $row->{"value".(isset($field_value)?"s_id":"")} = $renderValues;
                        $renderRows = array_merge($renderRows, [$row->slug=>$row]);
                    }else{
                        $renderRows = array_merge($renderRows, [$row->slug=>$row->id]);
                    }
                }
                $table->{"row".(isset($field)?"s_id":"")} = $renderRows;
                $renderTables = array_merge($renderTables, [$table->slug=>$table]);
            }else{
                $renderTables = array_merge($renderTables, [["table_id"=>$table->id, "table_slug"=>$table->slug]]);
            }
        }
        if(isset($field_sub_value)){
            $returnedJson = $renderValues;
        }elseif(isset($field_value)){
            $returnedJson = [
                "$rowSlug"=>$renderRows["$rowSlug"]
            ];
        }elseif(isset($field)){
            $returnedJson = [
                "$fieldSlug"=>$renderTables["$fieldSlug"]
            ];
        }else{
            $returnedJson = [
                "$slug"=>$renderTables
            ];
        }
        $apiName = $userApi->name;
        return [
            "layout"=>"api.php",
            "api"=>$returnedJson
        ];
    }
    function show(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $api = new Api();
        $userApi = $api->getUserApi(htmlEncode($_SESSION["user"]["id"]));

        return [
            "view"=>"api/show.php",
            "apis"=>$userApi,
            "breadcumb"=>[
                "APIs"=>"api/show"
            ]
        ];
    }
    function showTable(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        if($this->noParams()){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        $id = htmlEncode($this->params[0]);
        $api = new Api();
        $apiInfo = $api->getApi($id);
        $apiName = $apiInfo->name;
        $tables = $api->getTables($id);
        $apiInfo->name = htmlDecode($apiInfo->name);
        $apiInfo->description = htmlDecode($apiInfo->description);
        return [
            "view"=>"api/showTable.php",
            "apid"=>$id,
            "apiName"=>$apiName,
            "api"=>$apiInfo,
            "tables"=>$tables,
            "breadcumb"=>[
                "APIs"=>"api/show",
                "Api $apiName"=>"#"
            ]
        ];
    }
    function showRow(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        if($this->noParams()){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        $table_id = htmlEncode($this->params[0]);
        $api = new Api();
        $table = $api->getTable($table_id);
        $table_name = $table->name;
        $apid = $table->api_id;
        $rows = $api->getRows($table_id);
        $apiInfo = $api->getApi($apid);
        $apiName = $apiInfo->name;
        $apiInfo->name = htmlDecode($apiInfo->name);
        $apiInfo->description = htmlDecode($apiInfo->description);
        return [
            "view"=>"api/showRow.php",
            "table"=>$table,
            "rows"=>$rows,
            "breadcumb"=>[
                "APIs"=>"api/show",
                "Api $apiName"=>"api/showTable/$apid",
                "Table $table_name"=>"#"
            ]
        ];
    }
    function showValue(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        if($this->noParams()){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        $id = htmlEncode($this->params[0]);
        $api = new Api();
        $row = $api->getRow($id);
        $rowName = $row->name;
        $values = $api->getValues($id);
        $tableInfo = $api->getTable($row->table_id);
        $tableName = $tableInfo->name;
        $tableId = $tableInfo->id;
        $apiInfo = $api->getApi($tableInfo->api_id);
        $apiName = $apiInfo->name;
        $apid = $tableInfo->api_id;
        $apiInfo->name = htmlDecode($apiInfo->name);
        $apiInfo->description = htmlDecode($apiInfo->description);
        return [
            "view"=>"api/showValue.php",
            "row"=>$row,
            "values"=>$values,
            "breadcumb"=>[
                "APIs"=>"api/show",
                "Api $apiName"=>"api/showTable/$apid",
                "Table $tableName"=>"api/showRow/$tableId",
                "Row $rowName"=>"#"
            ]
        ];
    }
    function create(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        return [
            "view"=>"api/create.php"
        ];
    }
    function make(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
        }
        $name = htmlEncode($_REQUEST["name"]);
        $description = htmlEncode($_REQUEST["description"]);
        $keywords = htmlEncode($_REQUEST["keywords"]);
        $userId = htmlEncode($_SESSION["user"]["id"]);
        $cluster = $this->getCluster();
        $error = [];
        if(strlen($name)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $lastId = $api->createApi($name, $description, $keywords, $userId, $cluster);
            header("location:".$GLOBALS["path"]."api/showTable/$lastId");
        }
        return [
            "view"=>"api/create.php",
            "error"=>$error,
            "name"=>$name,
            "description"=>$description,
            "keywords"=>$keywords,
            "breadcumb"=>[
                "APIs"=>"api/show"
            ]
        ];
    }
    function editApi(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $name = htmlEncode($_REQUEST["name"]);
        $description = htmlEncode($_REQUEST["description"]);
        $keywords = htmlEncode($_REQUEST["keywords"]);
        $userId = htmlEncode($_SESSION["user"]["id"]);
        $id = $_REQUEST["apid"];
        $slug = htmlEncode($_REQUEST["slug"]);
        $error = [];
        if($id != $this->params[0]){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        if(strlen($name)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $api->updateApi($name, $description, $keywords, $slug, $userId, $id);
        }
        header("location:".$GLOBALS["path"]."api/showTable/$id");
    }
    function editTable(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        if($this->noParams()){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        $name = htmlEncode($_REQUEST["name"]);
        $description = htmlEncode($_REQUEST["description"]);
        $userId = htmlEncode($_SESSION["user"]["id"]);
        $id = htmlEncode($_REQUEST["table_id"]);
        $slug = htmlEncode($_REQUEST["slug"]);
        $error = [];
        if($id != $this->params[0]){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        if(strlen($name)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $api->updateTable($name, $description, $slug, $userId, $id);
        }
        header("location:".$GLOBALS["path"]."api/showRow/$id");
    }
    function editRow(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        if($this->noParams()){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        $name = htmlEncode($_REQUEST["name"]);
        $description = htmlEncode($_REQUEST["description"]);
        $userId = htmlEncode($_SESSION["user"]["id"]);
        $id = htmlEncode($_REQUEST["row_id"]);
        $slug = htmlEncode($_REQUEST["slug"]);
        $error = [];
        if(strlen($name)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $api->updateRow($name, $description, $slug, $userId, $id);
        }
        header("location:".$GLOBALS["path"]."api/showValue/$id#content");
    }
    function editValue(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        if($this->noParams()){
            header("location:".$GLOBALS["path"]."api/show");
            die();
        }
        $value = htmlEncode($_REQUEST["value"]);
        $id = htmlEncode($_REQUEST["id"]);
        $row_id = htmlEncode($_REQUEST["row_id"]);
        $userId = $_SESSION["user"]["id"];
        if(strlen($value)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $api->updateValue($value, $id, $userId);
        }
        header("location:".$GLOBALS["path"]."api/showValue/$row_id#content");
    }
    function createTable(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $name = htmlEncode($_REQUEST["name"]);
        $description = htmlEncode($_REQUEST["description"]);
        $apid = htmlEncode($_REQUEST["apid"]);
        $error = [];
        if(strlen($name)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $lastId = $api->createTable($name, $description, $apid);
            header("location:".$GLOBALS["path"]."api/showRow/$lastId#content");
            die();
        }
        $tables = $this->showTable();
        return [
            "view"=>"api/showTable.php",
            "error"=>$error,
            "name"=>$name,
            "description"=>$description,
            "apid"=>$apid,
            "tables"=>$tables["tables"],
            "breadcumb"=>$tables->breadcump
        ];
    }    
    function createRow(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $name = htmlEncode($_REQUEST["name"]);
        $description = htmlEncode($_REQUEST["description"]);
        $table_id = htmlEncode($_REQUEST["tableid"]);
        $error = [];
        if(strlen($name)<3) array_push($error, "The name should have at least 3 characters");
        if(sizeof($error)==0){
            $api = new Api();
            $lastId = $api->createRow($name, $description, $table_id);
            header("location:".$GLOBALS["path"]."api/showValue/$lastId");
            die();
        }
        $rows = $this->showRow();
        return [
            "view"=>"api/showRow.php",
            "error"=>$error,
            "name"=>$name,
            "description"=>$description,
            "tableid"=>$table_id,
            "rows"=>$rows["rows"],
            "breadcumb"=>$rows->breadcump
        ];
    }
    function createValue(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $value = htmlEncode($_REQUEST["value"]);
        $row_id = htmlEncode($_REQUEST["row_id"]);
        $error = [];
        if(strlen($value)<1) array_push($error, "The value should have at least one character");
        if(sizeof($error)==0){
            $api = new Api();
            $api->createValue($value, $row_id);
            header("location:".$GLOBALS["path"]."api/showValue/$row_id");
            die();
        }
        $rows = $this->showValue();
        return [
            "view"=>"api/showValue.php",
            "error"=>$error,
            "value"=>$value,
            "rowid"=>$row_id,
            "values"=>$values["values"],
            "breadcumb"=>$values->breadcump
        ];
    }
    function deleteValue($vid = null, $rid = null){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $api = new Api();
        $id = $vid ?? $_REQUEST["id"];
        $rowid = $rid ?? $_REQUEST["row_id"];
        $api->deleteValue($id, $_SESSION["user"]["id"]);
        if(!isset($vid)) {
            header("location:".$GLOBALS["path"]."api/showValue/$rowid");
        }
    }
    function deleteRow($rid = null, $tid = null){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $api = new Api();
        $id = $rid ?? $_REQUEST["id"];
        $tabid = $tid ?? $_REQUEST["table_id"];
        $userid=$_SESSION["user"]["id"];
        $values = $api->getValues($id);
        foreach($values as $value){
            $this->deleteValue($value->id, $id);
        }
        $api->deleteRow($id, $userid);
        if(!isset($rid)){
            header("location:".$GLOBALS["path"]."api/showRow/$tabid");
        }
    }
    function deleteTable($tid = null, $apid = null){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }
        $api = new Api();
        $id = $tid ?? $_REQUEST["id"];
        $apid = $apid ?? $_REQUEST["apid"];
        $userid=$_SESSION["user"]["id"];
        $rows= $api->getRows($id);
        foreach($rows as $row){
            $this->deleteRow($row->id, $id);
        }
        $api->deleteTable($id, $userid);
        if(!isset($tid)){
            header("location:".$GLOBALS["path"]."api/showTable/$apid");
            die();
        }
    }
    function deleteApi(){
        if(!isUserConnected()){
            header("location:".$GLOBALS["path"]."");
            die();
        }        
        $api = new Api();
        $id = $_REQUEST["id"];
        $userid=$_SESSION["user"]["id"];
        $tables= $api->getTables($id);
        foreach($tables as $table){
            echo "<br>API > id:".$table->id." nom:".$table->name."<br>";
            $this->deleteTable($table->id, $id);
        }
        $api->deleteApi($id, $userid);
        header("location:".$GLOBALS["path"]."api/show");
    }
}
