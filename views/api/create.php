<form action="<?= $GLOBALS["path"] ?>api/make" method="POST">
    <div class="form-group">
        <label for="name">Name</label>
        <input value="<?= isset($data->name)?$data->name:''; ?>" type="text" required class="form-control" name="name" id="name" placeholder="name">
    </div>
  <div class="form-group">
    <label for="description">Description</label>
    <textarea class="form-control" required id="description" rows="3" name="description"><?= isset($data->description)?$data->description:''; ?></textarea>
  </div>
  <div class="form-group">
    <label for="keywords">Keywords</label>
    <input value="<?= isset($data->keywords)?$data->keywords:''; ?>" type="text" required class="form-control" id="keywords" aria-describedby="keywordsHelp" name="keywords" placeholder="words, animals, things...">
    <small id="keywordsHelp" class="form-text text-muted">Separate keywords by comma ";"</small>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary">Create API</button>
  </div>
</form>