<div class="card-group">
    <div class="container px-lg-5">
        <div class="row justify-content px-lg-5">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title"><?= (sizeof($data->apis)>0)?"Ajouter une nouvelle API":"Aucune API" ?></h5>
                </div>
                <div class="card-body">
                    <p class="card-text">
                        <?= (sizeof($data->apis)>0)?"Vous en voulez plus ?":"Il y a un début à tout !" ?></p>
                    <a href="<?= $GLOBALS["path"] ?>api/create" type="button"
                        class="btn btn-primary"><?= (sizeof($data->apis)>0)?"Lancez vous !":"Ma première API" ?></a>
                </div>
            </div>
            <?php if(isset($data->apis)): ?>
            <?php foreach($data->apis as $userApi): ?>
            <div class="card col col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card-header">
                    <h5 class="card-title"><?= htmlDecode($userApi->name) ?></h5>
                </div>
                <div class="card-body">
                    <p class="card-text"><?= htmlDecode($userApi->description) ?></p>
                    <div class="d-flex flex-row justify-content-between">
                        <a href="<?= $GLOBALS["path"] ?>v1/<?php echo $userApi->id."-".$userApi->slug; ?>"
                            class="btn btn-success col-5">Voir</a>
                        <a href="<?= $GLOBALS["path"] ?>api/showTable/<?= $userApi->id ?>"
                            class="btn btn-primary col-5">Modifier</a>
                    </div>
                    <form action="<?= $GLOBALS["path"] ?>api/deleteApi/<?= $api->id ?>" method="post" class="">
                        <input type="submit" value="Supprimer" class="form-control btn btn-danger mt-3">
                        <input type="hidden" name="id" value="<?= $userApi->id ?>">
                    </form>
                    <p class="card-text"><small class="text-muted">Last updated <?= $userApi->update_date; ?></small>
                    </p>
                </div>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>