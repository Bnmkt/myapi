<h1>Modifier la Row <?= $data->row->name; ?></h1>
<ul class="nav nav-pills" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="info-tab" data-toggle="pill" href="#info" role="tab" aria-controls="info"
            aria-selected="true">Information</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="content-tab" data-toggle="pill" href="#content" role="tab" aria-controls="content"
            aria-selected="false">Contenu</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
        <form action="<?= $GLOBALS["path"] ?>api/editRow/<?= $data->row->id; ?>" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?= isset($data->row->name)?$data->row->name:''; ?>" type="text" required
                    class="form-control" name="name" id="name" placeholder="name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" required id="description" rows="3"
                    name="description"><?= isset($data->row->description)?$data->row->description:''; ?></textarea>
            </div>
            <div class="form-group">
                <label for="slug">Slug</label>
                <input class="form-control" required id="slug" pattern="[a-zA-Z0-9\-]+"
                    title="Only alphanumeric character ans dash" name="slug"
                    value="<?= isset($data->row->slug)?$data->row->slug:''; ?>">
            </div>
            <div class="col hidden">
                <input type="hidden" name="row_id" value="<?= $data->row->id; ?>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control mt-3">Update Row</button>
            </div>
        </form>
    </div>
    <div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content-tab">
        <div class="card-group">
            <div class="container">
                <div class="row">
                    <div class="card col-12">
                        <div class="card-header">Ajouter une valeur à <?= $data->row->name; ?></div>
                        <div class="card-body">
                            <div class="row">
                                <form action="<?= $GLOBALS["path"] ?>api/createValue/<?=$data->row->id;?>"
                                    method="POST">
                                    <div class="col">
                                        <label for="value">Valeur</label>
                                        <input type="text" placeholder="Nouvelle valeur" id="value" name="value"
                                            class="form-control" value="<?= $data->value; ?>">
                                    </div>
                                    <div class="col hidden">
                                        <input type="hidden" name="row_id" value="<?= $data->row->id; ?>">
                                    </div>
                                    <div class="col">
                                        <input type="submit" class="btn btn-primary form-control" id="submit"
                                            value="Ajouter une valeur">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h2>Liste des valeurs de <?= $data->row->name; ?></h2>
                        <ul class="list-group list-group-flush">
                            <?php if(isset($data->values) && sizeof($data->values)!=0): ?>
                            <?php foreach($data->values as $value): ?>
                            <li class="card col-12">
                                <div class="card-body flex flex-row d-flex justify-content-around">
                                        <input type="text" name="value" id="value" class="disabled" disabled
                                            placeholder="valeur" value="<?= $value->value; ?>">
                                    <form action="<?= $GLOBALS["path"] ?>api/editValue/<?= $value->id ?>" method="post"
                                        class=" flex flex-row d-flex justify-content-around form-group form-control border-0 align-top pt-0">
                                        <input type="text" name="value" id="value" class="form-control"
                                            placeholder="valeur" value="<?= $value->value; ?>">
                                        <input type="submit" value="Modifier" class=" btn btn-secondary">
                                        <input type="hidden" name="id" value="<?= $value->id ?>">
                                        <input type="hidden" name="row_id" value="<?= $data->row->id; ?>">
                                    </form>
                                    <form action="<?= $GLOBALS["path"] ?>api/deleteValue/<?= $value->id ?>" method="post" class="">
                                        <input type="submit" value="Supprimer" class="form-control btn btn-danger">
                                        <input type="hidden" name="id" value="<?= $value->id ?>">
                                        <input type="hidden" name="row_id" value="<?= $data->row->id; ?>">
                                    </form>
                                </div>
                            </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <li class="card col-12">
                                <p>Aucune Value</p>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>