<h1> Modifier l'API <?= htmlDecode($data->api->name); ?></h1>
<ul class="nav nav-pills" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="info-tab" data-toggle="pill" href="#info" role="tab" aria-controls="info"
            aria-selected="true">Information</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="content-tab" data-toggle="pill" href="#content" role="tab" aria-controls="content"
            aria-selected="false">Contenu</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
        <form action="<?= $GLOBALS["path"] ?>api/editApi/<?= $data->apid; ?>" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?= isset($data->api->name)?htmlDecode($data->api->name):''; ?>" type="text" required
                    class="form-control" name="name" id="name" placeholder="name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" required id="description" rows="3"
                    name="description"><?= isset($data->api->description)?htmlDecode($data->api->description):''; ?></textarea>
            </div>
            <div class="form-group">
                <label for="keywords">Keywords</label>
                <input value="<?= isset($data->api->keywords)?$data->api->keywords:''; ?>" type="text" required
                    class="form-control" id="keywords" aria-describedby="keywordsHelp" name="keywords"
                    placeholder="words, animals, things...">
                <small id="keywordsHelp" class="form-text text-muted">Separate keywords by comma ";"</small>
            </div>
            <div class="form-group">
                <label for="slug">Slug</label>
                <input class="form-control" required id="slug" pattern="[a-zA-Z0-9\-]+"
                    title="Only alphanumeric character ans dash" name="slug"
                    value="<?= isset($data->api->slug)?$data->api->slug:''; ?>">
            </div>
            <div class="col hidden">
                <input type="hidden" name="apid" value="<?= $data->apid; ?>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control mt-3">Update API</button>
            </div>
        </form>
    </div>
    <div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content-tab">
        <div class="card-group">
            <div class="container">
                <div class="row">
                    <div class="card col-12">
                        <div class="card-header">Ajouter une table à <?= $data->api->name; ?></div>
                        <div class="card-body">
                            <div class="row">
                                <form action="/api/api/createTable/<?=$data->apid;?>" method="POST">
                                    <div class="col">
                                        <label for="name">Name</label>
                                        <input type="text" placeholder="Table name" id="name" name="name"
                                            class="form-control">
                                    </div>
                                    <div class="col  mt-3">
                                        <label for="description">Description</label>
                                        <input type="text" placeholder="Short description" id="description"
                                            name="description" class="form-control">
                                    </div>
                                    <div class="col">
                                        <input type="submit" class="btn btn-primary form-control mt-3" id="submit"
                                            value="Create Table">
                                    </div>
                                    <div class="col hidden">
                                        <input type="hidden" name="apid" value="<?= $data->apid; ?>">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5 mb-1">
                        <h2>Liste des tables de <?= $data->api->name; ?></h2>
                        <ul class="list-group list-group-flush">
                            <?php if(isset($data->tables) && sizeof($data->tables)!=0): ?>
                            <?php foreach($data->tables as $table): ?>
                            <li class="card col-12">
                                <div class="card-header">
                                    <h3><?=$table->name?></h3>
                                </div>
                                <div class="card-body row">
                                    <div class="small col"><?=$table->description?></div>
                                    <div class="col">
                                        <a href="/api/api/showRow/<?= $table->id ?>" class="btn btn-secondary">Modifier
                                            la table</a>
                                        <form action="<?= $GLOBALS["path"] ?>api/deleteTable/<?= $table->id ?>"
                                            method="post" class="">
                                            <input type="submit" value="Supprimer" class="form-control btn btn-danger">
                                            <input type="hidden" name="id" value="<?= $table->id ?>">
                                            <input type="hidden" name="apid" value="<?= $data->api->id; ?>">
                                        </form>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <li class="card col-12">
                                <p>Aucune Table</p>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>