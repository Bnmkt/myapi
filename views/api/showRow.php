<h1>Modifier la Table <?= $data->table->name; ?></h1>
<ul class="nav nav-pills" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="info-tab" data-toggle="pill" href="#info" role="tab" aria-controls="info"
            aria-selected="true">Information</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="content-tab" data-toggle="pill" href="#content" role="tab" aria-controls="content"
            aria-selected="false">Contenu</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
        <form action="<?= $GLOBALS["path"] ?>api/editTable/<?= $data->table->id; ?>" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input value="<?= isset($data->table->name)?$data->table->name:''; ?>" type="text" required
                    class="form-control" name="name" id="name" placeholder="name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" required id="description" rows="3"
                    name="description"><?= isset($data->table->description)?$data->table->description:''; ?></textarea>
            </div>
            <div class="form-group">
                <label for="slug">Slug</label>
                <input class="form-control" required id="slug" pattern="[a-zA-Z0-9\-]+"
                    title="Only alphanumeric character ans dash" name="slug"
                    value="<?= isset($data->table->slug)?$data->table->slug:''; ?>">
            </div>
            <div class="col hidden">
                <input type="hidden" name="table_id" value="<?= $data->table->id; ?>">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary form-control mt-3">Update Table</button>
            </div>
        </form>
    </div>
    <div class="tab-pane fade" id="content" role="tabpanel" aria-labelledby="content-tab">
        <div class="card-group">
            <div class="container">
                <div class="row">
                    <div class="card col-12">
                        <div class="card-header">Ajouter un champ à <?= $data->table->name; ?></div>
                        <div class="card-body">
                            <div class="row">
                                <form action="<?= $GLOBALS["path"] ?>api/createRow/<?=$data->table->id;?>"
                                    method="POST">
                                    <div class="col">
                                        <label for="name">Name</label>
                                        <input type="text" placeholder="Row name" id="name" name="name"
                                            class="form-control">
                                    </div>
                                    <div class="col">
                                        <label for="description">Description</label>
                                        <input type="text" placeholder="Short description" id="description"
                                            name="description" class="form-control">
                                    </div>
                                    <div class="col">
                                        <input type="submit" class="btn btn-primary form-control" id="submit"
                                            value="Create Row">
                                    </div>
                                    <div class="col hidden">
                                        <input type="hidden" name="tableid" value="<?= $data->table->id; ?>">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h2>Liste des champs de <?= $data->table->name; ?></h2>
                        <ul class="list-group list-group-flush">
                            <?php if(isset($data->rows) && sizeof($data->rows)!=0): ?>
                            <?php foreach($data->rows as $row): ?>
                            <li class="card col-12">
                                <div class="card-header">
                                    <h3><?=$row->name?></h3>
                                </div>
                                <div class="card-body row">
                                    <div class="small col"><?=$row->description?></div>
                                    <div class="col">
                                        <a href="<?= $GLOBALS["path"] ?>api/showValue/<?= $row->id ?>"
                                            class="btn btn-secondary">Modifier la row</a>
                                        <form action="<?= $GLOBALS["path"] ?>api/deleteRow/<?= $row->id ?>"
                                            method="post" class="">
                                            <input type="submit" value="Supprimer" class="form-control btn btn-danger">
                                            <input type="hidden" name="id" value="<?= $row->id ?>">
                                            <input type="hidden" name="table_id" value="<?= $data->table->id; ?>">
                                        </form>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <li class="card col-12">
                                <p>Aucune Row</p>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>