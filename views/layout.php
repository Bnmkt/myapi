<?php if(!isset($data)){ $data = []; } ?>
<?php $data = (object) $data; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page d'api</title>
    <link rel="stylesheet" href="<?= $GLOBALS["path"] ?>sources/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $GLOBALS["path"] ?>sources/styles.css">
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="<?= $GLOBALS["path"] ?>">My API</a>
                <?php if(isUserConnected()): ?>
                Bienvenue <?= $_SESSION["user"]["username"]; ?>
                <?php endif; ?>
                <button class="navbar-toggler float-right" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse text-right" id="navbarNav">
                    <ul class="navbar-nav text-right">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="<?= $GLOBALS["path"] ?>">Home</a>
                        </li>
                        <?php if(isUserConnected()): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $GLOBALS["path"] ?>api/show">My APIs</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= $GLOBALS["path"] ?>api/create">New API</a>
                        </li>  
                        <?php endif; ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Profile</a>
                            <div class="dropdown-menu">
                            <?php if(isUserConnected()): ?>
                            <a class="dropdown-item" href="#">My profile</a>
                            <a class="dropdown-item" href="<?= $GLOBALS["path"] ?>auth/edit">Edit profile</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?= $GLOBALS["path"] ?>auth/logout">Logout</a>
                            <?php else: ?>     
                            <a class="nav-link" href="<?= $GLOBALS["path"] ?>auth/login">Login</a>
                            <a class="nav-link" href="<?= $GLOBALS["path"] ?>auth/signin">Sign in</a>
                            <?php endif; ?>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item btn" aria-current="page"><a href="<?= $GLOBALS["path"] ?>">Home</a></li>
                <?php if(isset($data->breadcumb)): ?>
                <?php $i = 0; 
                    foreach($data->breadcumb as $text => $link): 
                    $i++; 
                ?>
                <li>
                    &nbsp;/&nbsp;<a href="<?= $GLOBALS["path"] ?><?= $link; ?>" class="btn <?= $i==sizeof($data->breadcumb)?'disabled':"" ?>"><?= $text; ?></a>
                </li>
                <?php
                    endforeach; ?>
                <?php endif; ?>
            </ol>
        </nav>
        </div>
    </header>
    <div class="page container">
        <?php if(sizeof($data->error)): ?>
        <div class="error">
            <?php foreach($data->error as $key => $error): ?>
            <p class="alert alert-warning" role="alert"><?= $error ?></p>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php require($data->view?$data->view:"default.php"); ?>
    </div>
    <script src="<?= $GLOBALS["path"] ?>sources/bootstrap.min.js"></script>
    <script src="<?= $GLOBALS["path"] ?>sources/jquery.js"></script><script>
    $('.dropdown-toggle').click(function(){
        const obj = $(this)
        obj.toggleClass("show")
        obj.next(".dropdown-menu").toggleClass("show")
    })
</script>
    <script src="<?= $GLOBALS["path"] ?>sources/script.js"></script>
</body>

</html>