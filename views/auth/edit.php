<?php
parse_str($data->user->options, $data->user->options);
?>
<img src="<?= $data->user->avatar ?>" alt="">
<form action="<?= $GLOBALS["path"] ?>auth/editProfile" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <div>
            <label for="email">Email</label>
        </div>
        <input type="mail" title="Format : anything@server.tld" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
            readonly id="email" name="email" placeholder="Email" value="<?= $data->user->email ?>">
    </div>
    <div class="form-group">
        <div><label for="">Username</label></div>
        <input type="text" value="<?= $data->user->username ?>" name="username">
    </div>
    <div class="form-group">
        <div><label for="">Avatar</label></div>
        <input type="file" value="<?= $data->user->avatar ?>" name="avatar">
    </div>
    <div class="form-group">
        <div>
            <label for="password">New password</label>
        </div>
        <input type="password" id="password" name="password" placeholder="Password">
        <div>
            <label for="password2">Confirm new password</label>
        </div>
        <input type="password" id="password2" name="password2" placeholder="Password confirm">
    </div>
    <div class="form-group">
        <div>Options</div>
        <div class="form-check">
            <label class="form-check-label" for="visibility">Public profile</label>
            <input type="checkbox" class="form-check-input" id="visibility" name="visibility" <?= $data->user->options["visibility"]=='1'?"checked":"" ?>>
        </div>
    </div>
    <input type="hidden" name="userid" value="<?= $_SESSION["user"]["id"] ?>">
    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Modifier">
    </div>
</form>