<h1>S'enregistrer</h1>
<form action="<?= $GLOBALS["path"] ?>auth/register" method="POST">
    <div class="form-group">
        <div>
            <label for="email">Email</label>
        </div>
        <input type="mail" title="Format : anything@server.tld" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required id="email" name="email" placeholder="Email" value="<?= isset($data->email)?$data->email:'' ?>">
    </div>
    <div class="form-group">
        <div>
            <label for="password">Password</label>
        </div>
        <input type="password" required id="password" name="password" placeholder="Password" value="<?= isset($data->pass)?$data->pass:'' ?>">
    </div>
    <div class="form-group">
        <div>
            <label for="password2">Password confirmation</label>
        </div>
        <input type="password" required id="password2" name="password2" placeholder="Password confirmation">
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Se connecter">
    </div>
</form>