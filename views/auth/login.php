<h1>Se connecter</h1>
<form action="<?= $GLOBALS["path"] ?>auth/connect" method="POST">
    <div class="form-group">
        <div>
            <label for="email">Email</label>
        </div>
        <input type="mail" title="Format : anything@server.tld" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required id="email" name="email" placeholder="Email">
    </div>
    <div class="form-group">
        <div>
            <label for="password">Password</label>
        </div>
        <input type="password" required id="password" name="password" placeholder="Password">
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Se connecter">
    </div>
</form>