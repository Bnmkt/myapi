<h1>Home</h1>
<?php if(isValid($_REQUEST["d"])): ?>
<pre><?php var_dump($_REQUEST["d"]) ?></pre>
<?php endif; ?>
<div class="row">
  <?php if(isUserConnected()): ?>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Vos APIs</h5>
        <p class="card-text">Vos 5 dernières APIs</p>
        <a href="<?= $GLOBALS["path"] ?>api/show" class="btn btn-primary">Voir toutes vos APIs</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Votre profil</h5>
        <p class="card-text">Modifier vos informations personnelles et vos préférences...</p>
        <a href="<?= $GLOBALS["path"] ?>auth/edit" class="btn btn-secondary">Voir mon profil</a>
      </div>
    </div>
  </div>
  <?php else: ?>
    <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Pas encore de compte ?</h5>
        <a href="<?= $GLOBALS["path"] ?>auth/signin" class="btn btn-primary">Enregistrez vous!</a>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Déjà inscrit ?</h5>
        <a href="<?= $GLOBALS["path"] ?>auth/login" class="btn btn-secondary">Connectez vous !</a>
      </div>
    </div>
  </div>
  <?php endif; ?>
</div>
