<?php
$request = explode("/", $_REQUEST["r"]);
$ressource = htmlEncode($request[0]);
$action = htmlEncode($request[1]);
$params = array_slice($request, 2);
$data = null;
$method = $_SERVER['REQUEST_METHOD'];

if($method === "POST" || $method === "GET"){
    require("routes.php");
    $action = $a = strlen($action)>0?$action:"view";
    $ressource = $r = strlen($ressource)>0?$ressource:"home";
    $m = $method;
    if(!in_array($m.'/'.$a.'/'.$r, $routes)){ die("Bad routes\naction = $a\nressource = $r"); }
    
    $controllerName = ucfirst($r)."Controller";
    require_once("controllers/".$controllerName.".php");
    $controller = new $controllerName($params);
    
    $data = call_user_func([$controller, $a]);

}else{
    die("[".$method."] Bad request");
}